<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tugas php-3</title>
</head>
<body>
<h1>tugas php-3</h1>
    <?php
    function tentukan_nilai($number)
    {
        $output = " ";
        if ($number >= 85 && $number <= 100) {
            $output .= "Sangat Baik";
        }else if ($number >= 70 && $number <= 85) {
            $output .= "Baik";
        }else if ($number >= 60 && $number <= 70) {
            $output .= "VCukup";
        }else {
            $output .= "kurang";
        }
        return "$output <br>";
    }

    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang
    ?>

    <?php
        function ubah_huruf($string)
        {
            $abjad = "abcdefghijklmnopqrstuvwxyz";
            $output = "";

            for ($i = 0; $i < strlen($string); $i++) {
                $position = strpos($abjad,$string[$i]);
                $output .= substr($abjad, $position + 1, 1);
            }
            return "$output <br>";
        }
        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo ubah_huruf('developer'); // efwfmpqfs
        echo ubah_huruf('laravel'); // mbsbwfm
        echo ubah_huruf('keren'); // lfsfo
        echo ubah_huruf('semangat'); // tfnbohbu

    ?>

    <?php
    function tukar_besar_kecil($string)
    {
        $abjad = "abcdefghijklmnopqrstuvwxyz";
            $output = "";

        for ($a = 0; $a < strlen($string); $a++) {
            $apakah_huruf_kecil = strpos($abjad, $string[$a]);
            if ($apakah_huruf_kecil == null) {
                $output .= strtolower($string[$a]);
            }else {
                $output .= strtoupper($string[$a]);
            }
        }
        return "$output <br>";
    }

        // TEST CASES
        echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
        echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay" 
        echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
        echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
        echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

    ?>
</body>
</html>